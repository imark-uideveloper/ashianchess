$(document).ready(function () {

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            $(".header-wrapper").addClass("fixed-header");
        } else {
            $(".header-wrapper").removeClass("fixed-header");
        }
    });

    /*----- Slick Slider -----*/

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        arrows: false,
        centerMode: true,
        focusOnSelect: true
    });

    /*----- Interviews Slick Slider -----*/

    $('.interview-slider').slick({
        dots: true,
        infinite: true,
        speed: 800,
        autoPlay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>',
        nextArrow: '<a class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    /*----- Interviews Accordian Section ----*/

    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('fa fa-plus fa fa-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);


    jQuery('.select-box select').change(function () {
        if (jQuery(this).children('option:first-child').is(':selected')) {
            jQuery(this).addClass('selectpicker');
        } else {
            jQuery(this).removeClass('selectpicker');
        }
    });

    jQuery('.post').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
        offset: 100
    });
});

$(function (jQuery) {
    $('.firstCap').on('keypress', function (event) {
        var $this = $(this),
            thisVal = $this.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        $(this).val(FLC + con);
    });
});
$(function (jQuery) {
    jQuery("#uploadbrowsebutton").click(function () {
        jQuery('#fileuploadfield').click()
    });

    /*To bring the selected file value in text field*/
    jQuery('#fileuploadfield').change(function () {
        jQuery('#uploadtextfield').val($(this).val());
    });
});
